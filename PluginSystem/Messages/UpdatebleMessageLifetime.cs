﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PluginSystem.Messages
{
    /// <summary> Nachricht, die sich nach einer Zeitspanne selbst löscht.
    /// Wird der Inhalt aktualisiert, startet die Zeitspanne neu
    /// </summary>
    public class UpdatebleMessageLifetime:UpdateableMessage
    {
        //Lebenszeit der Nachricht
        TimeSpan _span;
        //Task, der das Löschen der Nachricht übernimmt
        Task deleteTask;
        //Abbruch für den Löschtask
        CancellationTokenSource cancelDelete;

        /// <summary> Ändere die Nachricht.
        /// </summary>
        public override string Message
        {
            get
            {
                return base.Message;
            }

            protected set
            {
                base.Message = value;
                resetTimeout();
            }
        }

        /// <summary> Erzeuge Nachricht, die sich nach einer Zeitspanne selbst löscht
        /// </summary>
        /// <param name="level">Level der Nachricht</param>
        /// <param name="startMessage">Start Message der Nachricht</param>
        /// <param name="source">Quelle der Nachricht</param>
        /// <param name="lifetime">Nach diese Zeit löscht sich die Nachricht selbst. Wird bei aktualisieren der Message neu gestartet</param>
        public UpdatebleMessageLifetime(MessageLevel level, string startMessage, PluginComponente source, TimeSpan lifetime)
            : base(level,startMessage, source)
        {
            _span = lifetime;
            resetTimeout();
            this.updated += UpdatebleMessageLifetime_updated;
        }

        /// <summary> Der Zustand der Nachricht hat sich geändert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdatebleMessageLifetime_updated(object sender, EventArgs e)
        {
            //Soll die Nachricht gelöscht werden
            if(deleteRequested)
            {
                //Breche Löschtask ab
                cancelDelete.Cancel();
            }
        }

        /// <summary> Setzte Timeout zurück. Starte Lösch Task neu
        /// </summary>
        private void resetTimeout()
        {
            cancelDelete?.Cancel();
            cancelDelete = new CancellationTokenSource();
            CancellationToken cancelAction = cancelDelete.Token;

            deleteTask = Task.Delay(_span).ContinueWith(t =>
            {
                if(!cancelAction.IsCancellationRequested)
                    this.delete();
            });
        }
    }
}
