﻿using PluginSystem.Messages;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PluginSystem.Messages.Controls
{
    /// <summary>
    /// Interaktionslogik für StackMessageDisplay.xaml
    /// </summary>
    public partial class StackMessageDisplay : UserControl
    {
        public readonly MessageReceiver toDisplay = new MessageReceiver();

        PluginMessage.MessageLevel[] _displayLevels;

        public PluginMessage.MessageLevel[] DisplayLevels
        {
            set
            {
                _displayLevels = value;
                updateDisplay();
            }
            get
            {
                return _displayLevels;
            }
        }

        public StackMessageDisplay()
        {
            InitializeComponent();
            dgInfo.ItemsSource = toDisplay.messages;
            DisplayLevels = new PluginMessage.MessageLevel[]
            {
                PluginMessage.MessageLevel.Info,
                PluginMessage.MessageLevel.Warning,
                PluginMessage.MessageLevel.Error
            };

            toDisplay.messagesChanged += ToDisplay_messagesChanged;
        }

        private void ToDisplay_messagesChanged(object sender, EventArgs e)
        {
            updateDisplay();
        }

        void updateDisplay()
        {
            dgInfo.Dispatcher.Invoke(() => dgInfo.ItemsSource = toDisplay.messages.Where(m => (m!=null)?DisplayLevels.Contains(m.Level):false));
        }
    }
}
