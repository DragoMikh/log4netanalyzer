﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace PluginSystem.View
{
    class TreeViewBoxItem:TreeViewItem
    {
        public readonly PluginToolbox bound;

        public TreeViewBoxItem(PluginToolbox box):base()
        {
            this.Header = box.name;
            this.ToolTip = box.description;
            this.bound = box;
        }
    }
}
