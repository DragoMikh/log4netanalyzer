﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Filter;
using ExtendedFilters.SelectLevel;
using ExtendedFilters.SelectRegex;
using ExtendedFilters.TimeSpan;
using ExtendedFilters.ContainsText;
using PluginSystem;

namespace ExtendedFilters
{
    public class ExtendedFiltersPlugin: PluginToolbox
    {
        public ExtendedFiltersPlugin():base("Extended Filters", "Filters with Selection Masks for more complex Filteroperations",
            new SelectLevels(),
            new TimeSpanFilter(),
            new RegexFilter(),
            new ContainsTextFilter())
        { }
    }
}
