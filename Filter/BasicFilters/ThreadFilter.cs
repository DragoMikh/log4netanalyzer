﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Filter;
using LogAnalyzerLib.Reader;

namespace BasicFilters
{
    /// <summary> Finde alle Einträge des Threads des gewählten Eintrags
    /// </summary>
    class ThreadFilter : Filter
    {
        public ThreadFilter():base("Thread Filter", "Filters for all Logs from one Thread",true)
        { }

        /// <summary> Finde alle Einträge, mit der ThreadId des gewählten Eintrags
        /// </summary>
        /// <param name="logsfile"></param>
        /// <param name="source"></param>
        /// <param name="additional"></param>
        /// <returns></returns>
        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            if (source == null)
                throw new ArgumentNullException();

            int thread = source.thread;

            return logsfile.Where(e => e.thread == thread).ToList();
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            return "All from Thread " + source.thread;
        }
    }
}
