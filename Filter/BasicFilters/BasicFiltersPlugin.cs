﻿using LogAnalyzerLib.Filter;
using PluginSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicFilters
{
    /// <summary> Enthält Werkzeuge für die grundelegende Log Analyse
    /// </summary>
    public class BasicFiltersPlugin: PluginToolbox
    {
        public BasicFiltersPlugin():base("Basic Filters", "Filters for handling basic filter operations",
            new ThreadFilter(),
            new LevelFilter(),
            new ExcludeLevelFilter(),
            new ExcludeIdenticalFilter())
        { }
    }
}