﻿using LogAnalyzerLib.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LogAnalyzerLib.Reader;

namespace BasicFilters
{
    class ExcludeIdenticalFilter:Filter
    {
        private const int maxDisplay = 17;

        public ExcludeIdenticalFilter()
            : base("Exclude Identical","Excludes Entries with Identical Message",true)
        {
        }

        public override string getDescriptionForCase(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            return "Message is not \""
                + (source.message.Length > maxDisplay ? source.message.Substring(0, maxDisplay) + "..." :source.message) + "\"";
        }

        protected override List<LogEntry> apply(List<LogEntry> logsfile, LogEntry source, object additional)
        {
            return logsfile.Where(l => l.message != source.message).ToList();
        }
    }
}
