﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LogAnalyzer
{
    class FilterTree
    {

        List<FilterTreeItem> filter = new List<FilterTreeItem>();

        public void Add(FilterTreeItem item)
        {
            filter.Add(item);
        }

        public void updateActive(List<LogEntry> logsfile, LogEntry selected)
        {
            foreach (FilterTreeItem item in filter)
                item.udpateActive(logsfile, selected);
        }
    }
}
