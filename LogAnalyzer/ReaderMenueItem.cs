﻿using LogAnalyzerLib.Reader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace LogAnalyzer
{
    class ReaderMenueItem:MenuItem
    {
        LogReader bound;

        public event EventHandler<LogReader> read;

        public ReaderMenueItem(LogReader binding) :base()
        {
            this.bound = binding;
            this.Header = bound.name;
            this.ToolTip = bound.description;
        }

        protected override void OnClick()
        {
            if (bound.configure())
            {
                bound.read();
                read?.Invoke(this, bound);
            }
        }
    }
}
