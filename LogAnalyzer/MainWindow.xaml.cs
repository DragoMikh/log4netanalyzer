﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LogAnalyzer;
using LogAnalyzerLib.Reader;
using LogAnalyzerLib.Filter;
using Microsoft.Win32;
using System.IO;
using LogAnalyzerLib;
using PluginSystem.Messages;
using PluginSystem;
using PluginSystem.Messages.Controls;
using PluginSystem.View;

namespace LogAnalyzer
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region data
        MessageSender _internalLog = new MessageSender("Log Analyzer");
        MessageSender _readerLog = new MessageSender("Log Reader");
        UpdateableMessage _interfaceWarning;

        List<MenuItem> _filterContextMenue = new List<MenuItem>();

        FilterMenue _filters = new FilterMenue();

        FilterTree _filtersTree = new FilterTree();

        /// <summary> Loader für Plugins
        /// </summary>
        ToolboxLoader _loader;

        /// <summary> Aktueller Suchstring
        /// </summary>
        string _searchTerm = string.Empty;
        /// <summary> Treffer der String Suche
        /// </summary>
        List<int> _searchIndex = new List<int>();
        #endregion

        #region Properties
        string Warning
        {
            set
            {
                lbWarn.Dispatcher.InvokeAsync(() =>
                {
                    if (value == string.Empty)
                        lbWarn.Background = Brushes.Transparent;
                    else
                    {
                        lbWarn.Background = Brushes.Red;
                        lbWarn.Foreground = Brushes.Yellow;
                    }

                    lbWarn.Content = value;
                    lbWarn.UpdateLayout();
                    lbWarn.InvalidateVisual();
                });
            }
        }
        string Info
        {
            set
            {
                lbInfo.Dispatcher.InvokeAsync(() =>
                {
                    lbWarn.Background = Brushes.Transparent;
                    if (value != string.Empty)
                    {
                        lbWarn.Background = Brushes.Transparent;
                        lbWarn.Foreground = Brushes.Black;
                    }

                    lbWarn.Content = value;
                    lbWarn.UpdateLayout();
                    lbWarn.InvalidateVisual();
                });
            }
        }
        #endregion

        #region ctor
        public MainWindow()
        {
            try
            {
                InitializeComponent();
            }
            catch(Exception e)
            {
                _internalLog.Send(new PluginMessage(PluginMessage.MessageLevel.Error, "Error at Window initialization", null));
            }

            //Initialisiere Plugin Loader und lade Plugins
            _loader = new ToolboxLoader(".//Plugins",true);
            //Verarbeite Übergabeparameter
            processCommands(Environment.GetCommandLineArgs());
            //Hänge Loader ins Message System
            attachLoaderToMessage();
            //Erzeuge Menüs
            generatePluginMenue();
            generateLoaderMenue();

            //Aboniere Events
            //Log Level der Filterstack Meldungs Aussgabe geändert
            mssStatus.LevelSelectionChanged += FssStatus_LevelSelectionChanged;
            //Aktuell gewählter Logeintrag geändert
            lgdLog.selectedLogEntryChanged += LgdLog_selectedLogEntryChanged;
            //Die Filter auf dem Filterstack haben sich geändert
            lgdLog.filterStackChanged += Filter_stackChanged;
            //Der Filterstack hat einen Filtervorgang abgeschlossen
            lgdLog.filter.finished += Filter_finished;

            //Übergebe Filterstack an Filterstack Meldungs Anzeige
            mssStatus.ToDisplay = smdMessages.toDisplay;

            //Verbinde LogAnzeige mit Message System
            smdMessages.toDisplay.AddSender(lgdLog.statusMessages);
            //Verbinde Filter mit Message System
            smdMessages.toDisplay.AddSender(lgdLog.filter.messages);
            //Verbinde Internen Message Sender mit Message System
            smdMessages.toDisplay.AddSender(_internalLog);
            //Verbinde Message Sender für Reader mit Message System
            smdMessages.toDisplay.AddSender(_readerLog);

            //Blende Filterstack Meldungs Anzeige aus            
            gdFilterMessages.Visibility = Visibility.Collapsed;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Aktualisiere Sichtbarkeit der Oberflächen Elemente
            updateVisibility();
        }
        #endregion

        #region Events
        private void miOpen_Click(object sender, RoutedEventArgs e)
        {
            //Erstelle Dialog zur Dateiauswahl
            OpenFileDialog diag = new OpenFileDialog();

            //Liste aller DateiFilter für alle FileReader
            List<string> filterString = new List<string>();
            //Gehe alle geladenen FilerReader durch
            foreach(FileReader reader in _loader.getAll<FileReader>())
            {
                //Lese aus, welche Dateine diese Verarbeiten können
                filterString.Add(reader.filesToRead);
            }

            //Verbinde Filter
            string filter = string.Join("|", filterString);            
            diag.Filter = filter;
            
            //Zeige Dateiauswahl
            bool? res = diag.ShowDialog();
            if (res.HasValue && res.Value)
            {
                //Lese Datei ein
                readFile(diag.FileName);
            }
        }

        private void miClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary> Nutzer hat einen Logeintrag zur Anzeige ausgewählt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LgdLog_selectedLogEntryChanged(object sender, LogEntry e)
        {
            ledLog.toDisplay = e;
            updateTreeMenue();
        }

        /// <summary> Nuter hat über das Filtermenü einen Filter gewählt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Menue_filterSelected(object sender, Filter e)
        {
            bool apply;
            object config = e.configure(lgdLog.sourceList, out apply);
            if (e.needsSource && lgdLog.SelectedItem == null)
            {
                Warning = "Filter '" + e.name + "' needs a selected Source Item";
            }
            else if (apply)
            {
                Mouse.OverrideCursor = Cursors.Wait;
                lgdLog.filter.Push(e, lgdLog.SelectedItem, config);
                if (lgdLog.Count <= 0)
                    Mouse.OverrideCursor = Cursors.Arrow;
            }
        }

        /// <summary> Die Filter auf dem Filterstack haben sich geändert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter_stackChanged(object sender, EventArgs e)
        {
            //Leere Anzeige
            lbFilterStack.Items.Clear();
            //Zeige Beschreibungen der Filter auf dem Stack an
            foreach (string s in lgdLog.filter.getView(lgdLog.sourceList))
                lbFilterStack.Items.Add(s);

            //Lösche Suche
            _searchTerm = string.Empty;

            //Aktualisiere Info Berreich
            updateInfo();
        }

        /// <summary> Lösche gesamten Filterstack
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miClearFilter_Click(object sender, RoutedEventArgs e)
        {
            lgdLog.resetView();
            lgdLog.filter.Clear();
        }
        /// <summary> Lösche obersten Filter vom Filterstack
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miRemoveFilter_Click(object sender, RoutedEventArgs e)
        {
            lgdLog.filter.Pop();
        }
        /// <summary> Springe zum nächsten Vorkommen des gesuchten Strings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbSearch_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Down || e.Key == Key.Enter)
                searchNext();
            if (e.Key == Key.Up)
                searchPrevious();
        }

        /// <summary> Springe zum vorherigen Vorkommen des gesuchten Strings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchPrevious_Click(object sender, RoutedEventArgs e)
        {
            searchPrevious();
        }

        /// <summary> Springe zum nächsten Vorkommen des gesuchten Strings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearchNext_Click(object sender, RoutedEventArgs e)
        {
            searchNext();
        }

        /// <summary> Level Auswahl der Filter Stack Message Anzeige hat sich geändert
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FssStatus_LevelSelectionChanged(object sender, FilterStackStatusLevelSelectionArgs e)
        {
            if (mssStatus.anySelected)
                gdFilterMessages.Visibility = Visibility.Visible;
            else
                gdFilterMessages.Visibility = Visibility.Collapsed;

            smdMessages.DisplayLevels = e.levels;
        }

        private void btnNextSuspicious_Click(object sender, RoutedEventArgs e)
        {
            nextSuspicious();
        }

        private void btnPrevoisSuspicious_Click(object sender, RoutedEventArgs e)
        {
            previousSuspicious();
        }

        /// <summary> Ein Filtervorgang wurde abgeschlossen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Filter_finished(object sender, int e)
        {
            Dispatcher.Invoke(() => Mouse.OverrideCursor = Cursors.Arrow);
            updateInfo(e);
        }

        /// <summary> Der Nutzer öffnet das Filtermenü
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miPlugins_SubmenuOpened(object sender, RoutedEventArgs e)
        {
            //Aktualisiere den Status der Filtereinträge im Menü
            _filters.updateActive(lgdLog.sourceList, lgdLog.SelectedItem);
        }

        /// <summary> Der Nutzer öffnet das Kontext Filtermenü
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lgdLog_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            //Gehe alle Toolboxen durch
            foreach (MenuItem mitem in _filterContextMenue)
            {
                //Gehe alle Filter durch
                foreach (FilterMenueItem fmi in mitem.Items)
                {
                    //Kann der Filter auf die aktuelle Auswahl angewandt werden
                    bool canApply = fmi.bound.canExecuteOnEntry(lgdLog.sourceList, lgdLog.SelectedItem);
                    fmi.IsEnabled = canApply;
                }
            }
        }

        /// <summary> Nutzer möchte die About Maske öffnen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void miAbout_Click(object sender, RoutedEventArgs e)
        {
            //Alle geladenen DLLs
            List<string> files = new List<string>();
            //Lese alle DLLs im Anwendungsverzeichnis ein
            files.AddRange(Directory.GetFiles(".", "*.dll", SearchOption.TopDirectoryOnly));
            //Existiert der Plugin Ordner
            if (Directory.Exists(".\\Plugins"))
            {
                //Lese DLLs im Plugin Ordner ein
                files.AddRange(Directory.GetFiles(".\\Plugins", "*.dll", SearchOption.TopDirectoryOnly));
            }
            //Zeige About Maske
            About about = new About(files.ToArray());
            about.ShowDialog();
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            bool open = btnAdd.IsChecked.GetValueOrDefault(false);
            popFilterSelection.IsOpen = open;
            updateTreeMenue();
        }

        private void reader_read(object sender, LogReader e)
        {
            //Signalisiere Nutzer, dass Programm beschäftigt ist
            Mouse.OverrideCursor = Cursors.Wait;
            e.logLoaded += Reader_logLoaded;
            //Lese Daten ein
            Task t = Task.Run(() =>
            {
                e.read();
            });
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Delete:
                    lgdLog.filter.Pop();
                    break;
            }
        }

        private void lgdLog_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.PageDown:
                    nextSuspicious();
                    break;

                case Key.PageUp:
                    previousSuspicious();
                    break;
            }
        }

        private void miPluginsInfo_Click(object sender, RoutedEventArgs e)
        {
            Window w = new Window();
            w.Icon = this.Icon;

            PluginOverview pov = new PluginOverview();
            w.Content = pov;
            w.SizeToContent = SizeToContent.WidthAndHeight;
            pov.toDisplay = _loader;

            w.ShowDialog();
        }
        #endregion

        #region helper functions
        private void attachLoaderToMessage()
        {
            foreach(var box in _loader.toolboxes)
                foreach(var loader in box.getPlugins<LogReader>())
                {
                    loader.messages = _readerLog;
                }
        }

        private void generatePluginMenue()
        {
            foreach(var box in _loader.toolboxes)
            {
                #region Generate Boxes
                MenuItem headerBoxItem = new MenuItem();
                headerBoxItem.Header = box.name;
                headerBoxItem.ToolTip = box.description;

                MenuItem contextBoxItem = new MenuItem();
                contextBoxItem.Header = box.name;
                contextBoxItem.ToolTip = box.description;

                TreeViewItem addButtonItem = new TreeViewItem();
                addButtonItem.Header = box.name;
                addButtonItem.ToolTip = box.description;
                #endregion
                #region Generate Filter Entries
                foreach (var filter in box.getPlugins<Filter>())
                {
                    FilterMenueItem menue = new FilterMenueItem(filter);
                    menue.filterSelected += Menue_filterSelected;
                    headerBoxItem.Items.Add(menue);
                    _filters.Add(menue);

                    if (filter.needsSource)
                    {
                        FilterMenueItem cmenue = new FilterMenueItem(filter);
                        cmenue.filterSelected += Menue_filterSelected;
                        contextBoxItem.Items.Add(cmenue);                        
                    }

                    FilterTreeItem tmenue = new FilterTreeItem(filter);
                    tmenue.filterSelected += Menue_filterSelected;
                    addButtonItem.Items.Add(tmenue);
                    _filtersTree.Add(tmenue);
                }
                #endregion
                if (headerBoxItem.Items.Count > 0)
                    miPlugins.Items.Add(headerBoxItem);
                if (contextBoxItem.Items.Count > 0)
                {
                    _filterContextMenue.Add(contextBoxItem);
                    lgdLog.ContextMenu.Items.Add(contextBoxItem);
                }
                if (addButtonItem.Items.Count > 0)
                {
                    tvFilterSelection.Items.Add(addButtonItem);
                }
            }
        }
        /// <summary> Erzeuge Menü für zusätzliche Importer
        /// </summary>
        private void generateLoaderMenue()
        {
            //Deaktiviere den Importer Menüpunkt. Dieser Zustand bleibt, wenn keine Importer geladen sind
            miImport.IsEnabled = false;
            //Gehe alle Reader durch, die geladen wurden
            foreach(var reader in _loader.getAll<LogReader>())
            {
                //Ist es kein FileReader, oder ein Importer
                if (!(reader is FileReader) ||
                    ((reader is FileReader && ((FileReader)reader).isImporter)))
                {
                    //Erzeuge neuen Menüpunkt
                    ReaderMenueItem it = new ReaderMenueItem(reader);
                    it.read += reader_read;
                    //Füge Menüpunkt hinzu
                    miImport.Items.Add(it);

                    //Aktiviere Importermenü, da es jetzt Optionen gibt
                    miImport.IsEnabled = true;                    
                }
            }
        }

        /// <summary> Verarbeite Übergabe Parameter
        /// </summary>
        /// <param name="commands"></param>
        private void processCommands(string[] commands)
        {
            List<string> boxes = new List<string>();
            foreach (string com in commands)
            {
                bool exists = File.Exists(com);
                string extension = System.IO.Path.GetExtension(com);

                if (exists && extension == ".xml")
                {
                    try
                    {
                        readFile(com);
                    }
                    catch(Exception e)
                    {
                        _internalLog.Send(new PluginMessage(PluginMessage.MessageLevel.Error, "Error at Loading File", null));
                        _internalLog.Send(new PluginMessage(PluginMessage.MessageLevel.Error, e.Message, null));
                    }
                }
                if (com.Contains("/toolbox:"))
                {
                    string box = com.Replace("/toolbox:","");
                    boxes.AddRange(findBoxes(box));
                }
            }
            foreach (string s in boxes)
                _loader.load(s);
        }
        private List<string> findBoxes(string path)
        {
            List<string> s = new List<string>();

            if (File.GetAttributes(path).HasFlag(FileAttributes.Directory))
            {
                foreach (string file in Directory.GetFiles(path, "*.dll"))
                {
                    s.Add(file);
                }
            }
            else
            {
                if (System.IO.Path.GetExtension(path) == "dll")
                    s.Add(path);
            }
            return s;
        }
        /// <summary> Lese Log Datei ein
        /// </summary>
        /// <param name="file"></param>
        private void readFile(string file)
        {
            bool loadedFile = false;

            Warning = string.Empty;            

            //Existiert die Datei überhaupt
            if (!File.Exists(file))
                throw new FileNotFoundException();
               
            //Gehe alle File Reader durch...
            foreach(var reader in _loader.getAll<FileReader>())
            {
                //Kann dieser mit der Datei was anfangen
                if (!reader.isImporter && reader.canProcess(file))
                {
                    _readerLog.Clear();

                    try
                    {
                        reader.logFile = file;
                        reader.logLoaded += Reader_logLoaded;
                        if (reader.configure())
                        {
                            //Signalisiere Nutzer, dass Programm beschäftigt ist
                            Mouse.OverrideCursor = Cursors.Wait;
                            Task t = Task.Run(() =>
                            {
                                reader.read();
                            });
                        }
                        loadedFile = true;
                    }
                    catch(OutOfMemoryException e)
                    {
                        Warning = "Logdatei zu groß";
                        _internalLog.Send(new PluginMessage(PluginMessage.MessageLevel.Warning, "Logdatei war zu groß!",null));
                    }
                    catch(Exception e)
                    {
                        Warning = e.Message;
                        _internalLog.Send(new PluginMessage(PluginMessage.MessageLevel.Error, "Fehler beim laden der Datei " + e.Message, null));
                    }
                    //Aktualisiere Fenster Kopf
                    this.Title = System.IO.Path.GetFullPath(file);
                    break;
                }
            }

            if(!loadedFile)
            {
                Warning = "Found no Reader in Plugins for this File";
            }
            else
            {
                //Aktualisiere Info Feld
                updateInfo();
                //Aktualisere Sichtbarkeit der Steuerelemente auf der Oberfläche
                updateVisibility();
            }

            //Signalisiere Nutzer, dass laden abgschlossen
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        //Ein Reader hat Logs geladen
        private void Reader_logLoaded(object sender, EventArgs e)
        {            
            LogReader reader = sender as LogReader;
            //Deaboniere Event, um mehrfach Abonnements zu verhindern
            reader.logLoaded -= Reader_logLoaded;

            lgdLog.Dispatcher.Invoke(() => {
                //Lösche Filterstack, da dieser für die neue Datei nicht gültig sein kann
                lgdLog.filter.Clear();

                //Zeige Eingelesene Daten an
                lgdLog.sourceList = reader.pullEntries();

                Mouse.OverrideCursor = Cursors.Arrow;
            });

            Info = "Datei erfolgreich geladen";
            updateInfo();
        }

        //Suche nach String in Logmeldungen
        #region search
        /// <summary> Suche vorheriges Vorkommen des gesuchten Strings
        /// </summary>
        private void searchPrevious()
        {
            updateSearchList();
            int index = findPrevious(lgdLog.SelectedIndex);
            if(index != -1)
                lgdLog.SelectedIndex = index;
        }
        /// <summary> Suche nächstes Vorkommen des gesuchten Strings
        /// </summary>
        private void searchNext()
        {
            updateSearchList();
            int index = findNext(lgdLog.SelectedIndex);
            if (index != -1)
                lgdLog.SelectedIndex = index;
        }
        /// <summary> Aktualisiere List mit Indexen der gesuchten Einträge
        /// </summary>
        private void updateSearchList()
        {
            if(tbSearch.Text != _searchTerm)
            {
                _searchTerm = tbSearch.Text;
                _searchIndex.Clear();
                _searchIndex.AddRange(lgdLog.sourceList.FindAllIndexof(l => l.message.Contains(_searchTerm)));
            }
        }
        /// <summary> Finde nächsten gesuchten Eintrag
        /// </summary>
        /// <param name="position">Start der Suche</param>
        /// <returns></returns>
        private int findNext(int position)
        {
            for(int i = 0; i < _searchIndex.Count; i++)
            {
                if (_searchIndex[i] > position)
                    return _searchIndex[i];
            }
            return -1;
        }
        /// <summary> Finde vorherigen gesuchten Eintrag
        /// </summary>
        /// <param name="position">Start der Suche</param>
        /// <returns></returns>
        private int findPrevious(int position)
        {
            for(int i = _searchIndex.Count - 1; i >= 0; i--)
            {
                if (_searchIndex[i] < position)
                    return _searchIndex[i];
            }
            return -1;
        }

        #endregion
        //Suche nach als auffällig markierten Einträgen
        #region suspicious
        /// <summary> Finde nächsten auffälligen Eintrag
        /// </summary>
        /// <param name="minSusLevel"></param>
        void nextSuspicious(int minSusLevel = 1)
        {
            if (lgdLog.Count > 0 && lgdLog.SelectedIndex < lgdLog.Count && lgdLog.sourceList.Any(e => e.suspicious > 0))
            {
                try
                {
                    int next = lgdLog.sourceList.FindIndex(lgdLog.SelectedIndex + 1, e => e.suspicious >= minSusLevel);
                    if (next != -1)
                        lgdLog.SelectedIndex = next;
                }
                catch(Exception e)
                {
                    if (_interfaceWarning != null)
                        _interfaceWarning.delete();

                    _interfaceWarning = new UpdatebleMessageLifetime(PluginMessage.MessageLevel.Error, "Can't jump to next entry", null, new TimeSpan(0, 0, 10));
                    _internalLog.Send(_interfaceWarning);
                }
            }
        }
        /// <summary> Finde vorherigen auffälligen Eintrag
        /// </summary>
        /// <param name="minSusLevel"></param>
        void previousSuspicious(int minSusLevel = 1)
        {            
            if (lgdLog.Count > 0 && lgdLog.SelectedIndex < lgdLog.Count && lgdLog.sourceList.Any(e => e.suspicious > 0))
            {
                try
                {
                    int previous = lgdLog.sourceList.FindLastIndex(0, lgdLog.SelectedIndex, e => e.suspicious >= minSusLevel);
                    if (previous != -1)
                        lgdLog.SelectedIndex = previous;
                }
                catch (Exception e)
                {
                    if (_interfaceWarning != null)
                        _interfaceWarning.delete();

                    _interfaceWarning = new UpdatebleMessageLifetime(PluginMessage.MessageLevel.Error, "Can't jump to previous entry", null, new TimeSpan(0, 0, 10));
                    _internalLog.Send(_interfaceWarning);
                }
            }
        }
        #endregion
        private void updateVisibility()
        {
            try
            {
                lgdLog.update();
            }
            catch
            { }
        }
        private void updateInfo(long number)
        {
            lbInfo.Dispatcher.Invoke(() =>
            {
                //Wenn Filter aktiv
                if (lgdLog.filter.Count > 0)
                    lbInfo.Content = number + " from " + lgdLog.Count + " entries";//Zeige wie viele Einträge Angezeigt werden und die Gesamtzahl der Einträge
                else//Kein Filter aktiv
                    lbInfo.Content = lgdLog.Count + " entries";//Zeige Gesamtzahl der Einträge
            });
        }
        private void updateInfo()
        {
            updateInfo(lgdLog.FilteredCount);
        }

        private void updateTreeMenue()
        {
            bool open = btnAdd.IsChecked.GetValueOrDefault(false);
            if (open)
                _filtersTree.updateActive(lgdLog.sourceList, lgdLog.SelectedItem);
        }
        #endregion
    }
}
