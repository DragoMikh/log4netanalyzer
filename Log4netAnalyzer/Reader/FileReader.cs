﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluginSystem.Messages;

namespace LogAnalyzerLib.Reader
{
    public abstract class FileReader : LogReader
    {
        /// <summary> Pfad der Logdatei
        /// </summary>
        private string _logFile;

        /// <summary> Unterstützte Dateiformate
        /// </summary>
        protected readonly string _filesToRead;

        /// <summary> Pfad der Logdatei
        /// </summary>
        public string logFile
        {
            get
            {
                return _logFile;
            }
            set
            {
                _logFile = value;
            }
        }
        /// <summary> Unterstützte Dateiformate
        /// </summary>
        public string filesToRead
        {
            get
            {
                return _filesToRead;
            }
        }

        /// <summary> Kann NICHT automatisch für eine Datei gewählt werden
        /// </summary>
        public readonly bool isImporter;

        /// <summary> Initialisiere File Reader
        /// </summary>
        /// <param name="name">Name des Readers</param>
        /// <param name="description">Beschreibung der Readers</param>
        /// <param name="filesToRead">Dateitypen die eingelesen werden </param>
        /// <param name="isImporter">Kann NICHT automatisch für eine Datei gewählt werden</param>
        public FileReader(string name, string description, string filesToRead, bool isImporter = false) : base(name, description)
        {
            this._filesToRead = filesToRead;
            this.isImporter = isImporter;
        }


        public void read(string file)
        {
            base.read();
            message(PluginMessage.MessageLevel.Info, "Read File " + file);
            _logFile = file;
            try
            {
                read();
            }
            catch (Exception e)
            {
                messages.Send(new PluginMessage(PluginMessage.MessageLevel.Error, e.Message + e.StackTrace, this));
            }
        }

        public override bool canProcess(string parameter)
        {
            if (!File.Exists(parameter))
                throw new FileNotFoundException("", parameter);

            bool found = false;
            string format = Path.GetExtension(parameter);
            string[] fileFormates = _filesToRead.Split('|');

            for(int i = 1; i < fileFormates.Length; i+=2)
            {
                string[] sub = fileFormates[i].Split(';');

                for (int j = 0; j < sub.Length; j++)
                {
                    if (sub[j] == "*" + format)
                        found = true;
                }
            }
            return found;
        }
    }
}
