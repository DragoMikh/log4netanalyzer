﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Log4netAnalyzer.Reader
{
    public class XMLReader:LogReader
    {
        private string _logFile;

        public string logFile
        {
            get
            {
                return _logFile;
            }
        }

        public XMLReader():base("XML Reader","Reads LogEntries from a XML File")
        {
        }
        public XMLReader(string logFile):this()
        {
            this._logFile = logFile;
        }

        public void read(string logFile)
        {
            try
            {
                _entries.Clear();
                this._logFile = logFile;
                read();
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public override void read()
        {
            try
            {
                loaded = false;

                string seri = File.ReadAllText(_logFile, Encoding.Default);
                seri = seri.Replace("log4net:", "");
                seri = "<log>" + seri + "</log>";

                XmlSerializer deser = new XmlSerializer(typeof(Log4NetXmlLogFile));

                using (var stream = new StringReader(seri))
                {
                    try
                    {
                        Log4NetXmlLogFile logFile = (Log4NetXmlLogFile)deser.Deserialize(stream);
                        for (long i = 0; i < logFile.entries.Count(); i++)
                            logFile.entries[i].id = i;

                        _entries.AddRange(logFile.entries);

                        loaded = true;
                    }
                    catch (Exception e)
                    {
                        // mach nix
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
