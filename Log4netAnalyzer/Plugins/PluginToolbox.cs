﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins
{
    /// <summary> Sammlung von Filtern
    /// </summary>
    public class PluginToolbox
    {
        /// <summary> Name der Toolbox
        /// </summary>
        public readonly string name;

        /// <summary> Beschreibung der Toolbox
        /// </summary>
        public readonly string description;

        /// <summary> Alle Filter in der Toolbox
        /// </summary>
        protected PluginComponente[] _tools;

        /// <summary> Gebe alle Filter in der Toolbox zurück
        /// </summary>
        public PluginComponente[] tools
        {
            get
            {
                return _tools;
            }
        }

        /// <summary> Erstelle neue Toolbox
        /// </summary>
        /// <param name="name"></param>
        /// <param name="tools">Alle Filter in der Toolbox</param>
        public PluginToolbox(string name, string description, params PluginComponente[] tools)
        {
            this.name = name;
            this.description = description;
            this._tools = tools;

            foreach (PluginComponente f in tools)
                f.toolbox = name;
        }

        public T[] getPlugins<T>()
        {
            var found = tools.Where(s => s is T);
            T[] converted = new T[found.Count()];

            int i = 0;
            foreach (var single in found)
                converted[i++] = (T)(object)single;

            return converted;
        }
    }
}
