﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins.Messages
{
    public class MessageReceiver
    {
        private List<PluginMessage> _messages = new List<PluginMessage>();

        public event EventHandler messagesChanged;

        public PluginMessage[] messages
        {
            get
            {
                return _messages.ToArray();
            }
        }

        public void AddSender(MessageSender sender)
        {
            sender.send += Sender_send;
            sender.clearSender += Sender_clearSender;
            sender.clearComponent += Sender_clearComponent;
        }

        private void Sender_clearComponent(object sender, PluginComponente e)
        {
            foreach (var m in _messages.FindAll(m => m.Source == e))
            {
                if (m is UpdateableMessage)
                {
                    var upmess = m as UpdateableMessage;
                    upmess.updated -= Upmess_updated;
                }
            }

            _messages.RemoveAll(m => m.Source == e);
            messagesChanged?.Invoke(this, null);
        }

        private void Sender_clearSender(object sender, EventArgs e)
        {
            foreach (var m in _messages.FindAll(m => m.Sender == sender))
            {
                if (m is UpdateableMessage)
                {
                    var upmess = m as UpdateableMessage;
                    upmess.updated -= Upmess_updated;
                }
            }
            _messages.RemoveAll(m => m.Sender == sender);
            messagesChanged?.Invoke(this, null);
        }

        private void Sender_send(object sender, PluginMessage e)
        {
            _messages.Add(e);

            if (e is UpdateableMessage)
            {
                var upmess = e as UpdateableMessage;
                upmess.updated += Upmess_updated;
            }

            messagesChanged?.Invoke(this, null);
        }

        private void Upmess_updated(object sender, EventArgs e)
        {
            messagesChanged?.Invoke(this, null);
        }

        public int messagesCount()
        {
            return _messages.Count;
        }
        public int messagesCount(PluginMessage.MessageLevel level)
        {
            return _messages.Count(m => m.Level == level);
        }
        public PluginMessage[] getMessages(PluginMessage.MessageLevel[] levels)
        {
            return messages.Where(m => contains(levels, m.Level)).ToArray();
        }

        private bool contains<T>(T[] array, T element)
        {
            foreach (T i in array)
                if (i.Equals(element))
                    return true;
            return false;
        }
    }
}
