﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins.Messages
{
    public class MessageSender
    {
        public event EventHandler<PluginMessage> send;
        public event EventHandler<PluginComponente> clearComponent;
        public event EventHandler clearSender;

        private readonly string _senderName;

        public string SenderName
        {
            get
            {
                return _senderName;
            }
        }

        public MessageSender(string name)
        {
            this._senderName = name;
        }

        public void Send(PluginMessage message)
        {
            message.Sender = this;
            send?.Invoke(this, message);
        }

        public void Clear()
        {
            clearSender?.Invoke(this, null);
        }
        public void Clear(PluginComponente component)
        {
            clearComponent?.Invoke(this, component);
        }

        public override string ToString()
        {
            return _senderName;
        }
    }
}
