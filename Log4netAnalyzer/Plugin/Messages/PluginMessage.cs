﻿using LogAnalyzerLib.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins.Messages
{
    /// <summary> Meldung aus einem Filter
    /// </summary>
    public class PluginMessage
    {
        public enum MessageLevel { Info, Warning, Error };

        public DateTime TimeStamp { get; }
        public MessageLevel Level { get; private set; }
        public virtual string Message { get; protected set; }
        public PluginComponente Source { get; private set; }
        public MessageSender Sender { get; internal set; }

        public PluginMessage(MessageLevel level, string message, PluginComponente source)
        {
            this.TimeStamp = DateTime.Now;
            this.Level = level;
            this.Message = message;
            this.Source = source;
        }
    }
}
