﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogAnalyzerLib.Plugins.Messages
{
    public class UpdateableMessage : PluginMessage
    {
        public event EventHandler updated;

        public string updateMessage
        {
            set
            {
                base.Message = value;
                updated?.Invoke(this, null);
            }
        }

        public UpdateableMessage(MessageLevel level, string startMessage, PluginComponente source) : base(level, startMessage, source)
        {
        }
    }
}
