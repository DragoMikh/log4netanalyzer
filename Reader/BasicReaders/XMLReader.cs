﻿using LogAnalyzerLib.Reader;
using PluginSystem.Messages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BasicReaders
{
    /// <summary> Reader zum einlesen von Log4net XML Datein
    /// </summary>
    class XMLReader:FileReader
    {
        private const double MAX_FILE_SIZE = 5;

        public XMLReader()
            : base("XML Files","Read XML Logfiles","Log4net XML Files|*.xml;*.XML")
        {
        }

        public override bool canProcess(string parameter)
        {
            if (!base.canProcess(parameter))
                return false;

            if (File.Exists(parameter))
            {
                var info = new FileInfo(parameter);
                double mb = info.Length / (Math.Pow(10.0, 6));

                if (mb <= MAX_FILE_SIZE)
                {
                    message(PluginMessage.MessageLevel.Warning, "File to large for XML Reader");
                    return true;                    
                }
                else
                    return false;
            }
            else
                return false;
        }

        public override void read()
        {
            messages.Clear(this);
            try
            {
                //Lösche alte Liste
                _entries.Clear();
                GC.Collect();

                loaded = false;

                string seri = File.ReadAllText(logFile, Encoding.Default);
                //Lösche Namespace Verweise
                seri = seri.Replace("log4net:", "");
                //Füge Root hinzu
                seri = "<log>" + seri + "</log>";

                //XmlSerialisierer zum Einlesen der XML Daten
                XmlSerializer deser = new XmlSerializer(typeof(Log4NetXmlLogFile));

                //Reader zum Lesen der Datei
                using (var stream = new StringReader(seri))
                {
                    try
                    {
                        message(PluginMessage.MessageLevel.Info, "Deserialize XML File");
                        //Lese die Daten aus dem XML String
                        Log4NetXmlLogFile logFile = (Log4NetXmlLogFile)deser.Deserialize(stream);
                        message(PluginMessage.MessageLevel.Info, "Number entries");
                        //Nummeriere Einträge durch
                        for (long i = 0; i < logFile.entries.Count(); i++)
                            logFile.entries[i].id = i;

                        //Speichere Einträge
                        _entries.AddRange(logFile.entries);
                        message(PluginMessage.MessageLevel.Info, "File read");
                        //Daten wurden geladen
                        loaded = true;
                    }
                    catch (Exception e)
                    {
                        message(PluginMessage.MessageLevel.Error, "An error occurred, while deserializing XML File");
                        message(PluginMessage.MessageLevel.Error, e.Message + e.StackTrace);
                    }
                }
            }
            catch (Exception e)
            {
                message(PluginMessage.MessageLevel.Error, e.Message + e.StackTrace);
            }

            GC.Collect();
        }
    }
}
