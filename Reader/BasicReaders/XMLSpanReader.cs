﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LogAnalyzerLib.Reader;
using System.Xml.Serialization;
using FilterUtilities;
using PluginSystem.Messages;

namespace BasicReaders
{
    public class XMLSpanReader : FileReader
    {
        readonly byte[] startMarker = Encoding.Default.GetBytes("<log4net:event");
        readonly byte[] endMarker = Encoding.Default.GetBytes("</log4net:event>");

        //Anfang des Einlese Zeitraus
        DateTime start;
        //Ende des Einlese Zeitraums
        DateTime end;

        public XMLSpanReader()
            : base("XML Span Reader", "Reads Logs from a Log4net XML File that were created in a certain timespan", "Log4net XML Files|*.xml;*.XML")
        {
        }

        public override bool canProcess(string parameter)
        {
            return base.canProcess(parameter);
        }

        public override bool configure()
        {
            messages.Clear(this);
            using (StreamReader reader = new StreamReader(this.logFile))
            {
                message(PluginMessage.MessageLevel.Info, "Start and End-Entry");
                //Hole ersten Eintrag der Logdatei
                string firstEntry = ReadEntry(reader.BaseStream);
                
                //Hole letzten Eintrag der Logdatei
                reader.BaseStream.Seek(-20, SeekOrigin.End);
                string lastEntry = ReadEntry(reader.BaseStream);

                message(PluginMessage.MessageLevel.Info, "Start and End-Time");
                //Lese Startzeit aus
                start = getTimeStamp(firstEntry);
   
                //Lese Endzeit aus
                end = getTimeStamp(lastEntry);

                //Frage Nutzer nach Zeitraum
                SelectTimeSpanMask selectTimespan = new SelectTimeSpanMask(start, end);
                selectTimespan.ShowDialog();

                //Hat der Nutzer einen Zeitraum gewählt
                if (selectTimespan.ApplyValue)
                {
                    //Lese Zeiten der Auswahl Maske aus
                    start = selectTimespan.StartTime;
                    end = selectTimespan.EndTime;
                    return true;
                }
                else
                    message(PluginMessage.MessageLevel.Warning, "Canceled Operation");

                return false;
            }
        }

        public override void read()        
        {
            //Setzte Filter zurück
            this.loaded = false;
            this._entries.Clear();
            //Bereinige Speicher
            GC.Collect();

            //Existiert die Zieldatei
            if (!File.Exists(this.logFile))
                throw new FileNotFoundException();

            //Ist die Datei auch wirklich eine XML Datei
            if (Path.GetExtension(this.logFile).ToLower() != ".xml")
                throw new FileFormatException();

            //Öffne Stream auf Datei
            using (StreamReader reader = new StreamReader(this.logFile))
            {
                message(PluginMessage.MessageLevel.Info, "Read span from file");
                //Lese Zeitraum ein
                readSpan(reader.BaseStream, start, end);
                base.loaded = true;
            }

            GC.Collect();
        }

        /// <summary> Lese Zeitspanne aus Logdatei ein
        /// </summary>
        /// <param name="file"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        private void readSpan(Stream file, DateTime start, DateTime end)
        {
            //Obergrenze bei Start Suche
            long topBorder = 0;
            //Untergrenze bei Start Suche
            long bottomBorder = file.Length;
            //Die Suche hat das Element gefunden
            bool found = false;
            //Position des ersten Elements
            long startPosition = 0;

            bool parse = true;

            _entries.Clear();

            message(PluginMessage.MessageLevel.Info, "Find first entry of span");
            //Finde ersten Eintrag
            do
            {
                try
                {
                    //Mitte des Suchbereichs
                    startPosition = topBorder + ((bottomBorder - topBorder) / 2);
                    //Lese Eintrag in der Mitte
                    string entry = ReadEntry(file, startPosition);
                    //Lese Timestamp des Eintrags
                    DateTime t = getTimeStamp(entry);

                    if (t < start)//Sind wir zu früh
                        topBorder = startPosition;//Nehme Position als neue Obergrenze
                    else if (t > start)//Sind wir zu spät
                        bottomBorder = startPosition;//Nehme Position als neue Untergrenze
                    else//Sind wir genau auf dem Zeitpunkt
                        found = true;//Wir haben den gesuchten Zeitpunkt gefunden
                }
                catch (Exception e)
                {
                    message(PluginMessage.MessageLevel.Error, "Error at search for first entry");
                    message(PluginMessage.MessageLevel.Error, e.Message + e.StackTrace);
                    throw e;
                }
            }
            while (!found && (bottomBorder - topBorder) > 1);//Solange der gesuchte Eintrag nicht gefunden wurde, oder die Suche auf 1 Element eingegrenzt hat

            message(PluginMessage.MessageLevel.Info, "Found start entry");

            //Serializer zum Einlesen der Log Daten
            XmlSerializer seri = new XmlSerializer(typeof(Log4NetXmlLogFile));
            //Springe in der Datei zum gefundenen StartEintrag
            file.Position = startPosition;

            message(PluginMessage.MessageLevel.Info, "Read entries from start");

            var progressMessage = updateableMessage(PluginMessage.MessageLevel.Info, "0 entries read");
            //Der wievielte Eintrag wurde eingelesen
            long position = 0;
            do
            {
                //Lese Log Eintrag
                string entry = ReadEntry(file);
                //Formatiere Eintrag zu einlesen durch den Serialisierer
                entry = "<log>" + entry.Replace("log4net:", "") + "</log>";
                //DeSerialisiere den Eintrag
                StringReader str = new StringReader(entry);
                var singleEntryPacked = (Log4NetXmlLogFile)seri.Deserialize(str);
                var singleEntry = singleEntryPacked.entries.First();

                //Setzte Position des Eintrags
                singleEntry.id = position;

                //Haben wir das Ende erreicht
                if (singleEntry.timestamp > end)
                    parse = false;//Beende einlesen
                else
                {
                    //Füge zu Einträgen hinzu
                    _entries.Add(singleEntry);
                    //Erhöhe Anzahl der eingelesenen Einträge
                    position++;

                    progressMessage.updateMessage = position + " entries read";
                }
            }
            while (parse);//Parse, solange gesagt wird

            message(PluginMessage.MessageLevel.Info, "Finished reading entries");
        }

        /// <summary> Lese Zeitstempel aus Logdatei
        /// </summary>
        /// <param name="entry"></param>
        /// <returns></returns>
        private DateTime getTimeStamp(string entry)
        {
            //Suche Anfang des Zeitstempels
            int start = entry.IndexOf("timestamp=") + 11;
            //Suche Ende des Zeitstempels
            int end = entry.IndexOf("\"", start + 11);

            //Schneide Zeistempel aus
            string part = entry.Substring(start, end - start);

            DateTime dt = DateTime.Now;
            //Parse Zeitstempel
            if (parseLog4netTimeStamp(part, out dt))
            {
                return dt;
            }
            return DateTime.Now;
        }
        /// <summary> Parse einen Log4net Zeistempel
        /// </summary>
        /// <param name="s"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        private bool parseLog4netTimeStamp(string s, out DateTime result)
        {
            string[] parts = s.Split('T');

            if (parts.Length != 2)
            {
                result = DateTime.Now;
                return false;
            }

            string[] dateParts = parts[0].Split('-');

            if (dateParts.Length != 3)
            {
                result = DateTime.Now;
                return false;
            }

            int year = Int32.Parse(dateParts[0]);
            int month = Int32.Parse(dateParts[1]);
            int day = Int32.Parse(dateParts[2]);

            string[] timeParts = parts[1].Split('+');
            string[] tpart = timeParts.First().Split('.');
            string[] ntparts = tpart.First().Split(':');

            if (tpart.Length != 2 && ntparts.Length != 3)
            {
                result = DateTime.Now;
                return false;
            }

            int ms = Int32.Parse(tpart.Last().Substring(0, 3));
            int sec = Int32.Parse(ntparts[2]);
            int min = Int32.Parse(ntparts[1]);
            int hr = Int32.Parse(ntparts[0]);

            result = new DateTime(year, month, day, hr, min, sec, ms);
            return true;
        }

        /// <summary> Lese den Eintrag an diese Position im Stream
        /// </summary>
        private string ReadEntry(Stream stream, long position)
        {
            //Speichere aktuelle Position im Stream
            long currentPosition = stream.Length;

            //Springe zu gewünschter Position
            stream.Position = position;
            //Lese Eintrag
            string entry = ReadEntry(stream);
            //Springe zu gespeicherter Position zurück
            stream.Position = currentPosition;

            //Gebe gelesenen Eintrag zurück
            return entry;
        }

        /// <summary> Lese Eintrag aus dem Stream, an aktueller Position
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        private string ReadEntry(Stream stream)
        {
            long startPosition = stream.Position;

            long start = searchIndex(stream, startPosition + startMarker.Length, startMarker, true);
            long end = searchIndex(stream, startPosition, endMarker, false);

            string fullText = stream.ReadString(start, end);

            stream.Position += fullText.Length;

            return fullText;
        }

        /// <summary> Suche Position eines Byte Musters in dem Übergebenen Stream
        /// </summary>
        /// <param name="source">Stream in dem Gesucht werden soll</param>
        /// <param name="startIndex">Start der Suche</param>
        /// <param name="toFind">Byte Muster das Gesucht werden soll</param>
        /// <param name="backwards">Wenn FALSE suche vorwärts, wenn TRUE suche rückwärts</param>
        /// <returns></returns>
        private static long searchIndex(Stream source, long startIndex, byte[] toFind, bool backwards = false)
        {
            //Schieberegister
            byte[] buffer = new byte[toFind.Length];
            //Position im Stream vor dem Ausführen der Suche
            long startPosition = source.Position;
            //Gefundene Position
            long position = -1;
            //Wurde etwas gefunden
            bool found = false;
            //Byte, das aus dem Stream gelesen wurde
            int bt;

            source.Position = startIndex;

            do
            {
                if (backwards)
                {
                    //Gehe im Stream ein Byte rückwärts und gebe diesen Wert zurück
                    bt = source.readBack();
                    //Convertiere zu Byte
                    byte b = (byte)bt;
                    //Schiebe Byte in Schieberegister
                    pushFront(buffer, b);
                }
                else
                {
                    bt = source.ReadByte();
                    byte b = (byte)bt;
                    pushBack(buffer, b);
                }

                string bufferDump = Encoding.UTF8.GetString(buffer);

                //Steht das Gesuchte Muster im Buffer?
                if (compareBytes(buffer, toFind))
                {
                    //Gefunden
                    position = source.Position;
                    found = true;
                }
            }
            while (!found && bt != -1);

            source.Position = startPosition;

            return position;
        }

        private static void pushFront(byte[] buffer, byte b)
        {
            Array.Copy(buffer, 0, buffer, 1, buffer.Length - 1);
            buffer[0] = b;
        }
        private static void pushBack(byte[] buffer, byte b)
        {
            Array.Copy(buffer, 1, buffer, 0, buffer.Length - 1);
            buffer[buffer.Length - 1] = b;
        }
        private static bool compareBytes(byte[] b1, byte[] b2)
        {
            if (b1.Length != b2.Length)
                return false;

            for (int i = 0; i < b1.Length; i++)
                if (b1[i] != b2[i])
                    return false;

            return true;
        }
    }

    /// <summary> Erweiterungen der Stream Klasse
    /// </summary>
    static class StreamExtension
    {
        /// <summary> Lese ein Byte vor der aktuellen Position und bewege die Position im Stream eins nach vorne
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int readBack(this Stream s)
        {
            if (s.Position == 0)
                return -1;

            s.Position--;
            int b = s.ReadByte();
            s.Position--;
            return b;
        }
        /// <summary> Lese einen Berreich aus einem Stream und wandle ihn in einen String um
        /// </summary>
        /// <param name="s"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static string ReadString(this Stream s, long start, long end)
        {
            long startPosition = s.Position;
            byte[] bytes = new byte[end - start];

            s.Position = start;

            s.Read(ref bytes, start);

            s.Position = startPosition;

            string str = Encoding.Default.GetString(bytes);
            return str;
        }
        /// <summary> Lese eine Menge Bytes aus dem Stream
        /// </summary>
        /// <param name="s"></param>
        /// <param name="buffer">Buffer zum befüllen</param>
        /// <param name="offset">Startposition</param>
        public static void Read(this Stream s, ref byte[] buffer, long offset)
        {
            long startPosition = s.Position;

            s.Position = offset;

            for (long l = 0; l < buffer.Length; l++)
            {
                buffer[l] = (byte)s.ReadByte();
            }

            s.Position = startPosition;
        }
    }
}